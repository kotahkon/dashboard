import axios, { AxiosInstance } from "axios";
import { Injectable } from "@angular/core";
import { LoadingBarService } from "@ngx-loading-bar/core";
import { environment } from 'src/environments/environment';
import { getAccessToken } from 'src/auth/auth-token';

const BASEURL = `http://${environment.baseURL}`;

@Injectable({
    providedIn: "root"
})
export class Requests {
    private axiosClient: AxiosInstance;

    constructor(
        private loadingBar: LoadingBarService,
    ) {
        this.axiosClient = axios.create({
            timeout: 15000,
            headers: {
                Accept: "application/json",
                Authorization: "jwt " + getAccessToken(),
            }
        });
    }

    public async get<T>(url, params?: Object): Promise<any> {
        try {
            this.loadingBar.start();
            var axiosResponse = await this.axiosClient.request({
                method: "GET",
                url: BASEURL + url,
                params: params
            });
            this.loadingBar.complete();
            return axiosResponse.data;
        } catch (error) {
            this.loadingBar.complete();
            return Promise.reject(error);
        }
    }

    public async post(url, data): Promise<any> {
        try {
            this.loadingBar.start();
            var axiosResponse = await this.axiosClient.request({
                method: "POST",
                url: BASEURL + url,
                data: data
            });
            this.loadingBar.complete();
            return axiosResponse.data;
        } catch (error) {
            this.loadingBar.complete();
            return Promise.reject(error);
        }
    }
}
