import { Component, OnInit } from '@angular/core';
import { Requests } from 'src/providers/requests';
import { setAccessToken, setRefreshToken } from 'src/auth/auth-token';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {
  loading = false;
  message = '';
  loginRequest = {
    username: '',
    password: '',
    password2: '',
    captcha: '',
  }
  valid_captcha = false;
  recaptchaSiteKey = environment.recaptchaSiteKey;

  constructor(
    private requests: Requests,
  ) {

  }

  resolved(ev) {
    this.valid_captcha = ev != null;
  }

  async submit() {
    this.message = '';
    if (this.loginRequest.password != this.loginRequest.password2) {
      this.message = 'Passwords are not the same as each other';
      return;
    }
    this.loading = true;
    let response = await this.requests.post("/auth/register", this.loginRequest);
    if (response.status != "ok") {
      this.loading = false;
      this.message = response.message;
      return;
    }

    setAccessToken(response.result.access_token);
    setRefreshToken(response.result.refresh_token);

    location.href = "/";    
  }

  ngOnInit() {
  }

}
