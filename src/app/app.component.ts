import { Component } from '@angular/core';
import { AuthGuardService } from 'src/auth/auth-gaurd.service';
import { Requests } from 'src/providers/requests';
import { getRefreshToken, delAccessToken, delRefreshToken } from 'src/auth/auth-token';
import { Router, NavigationEnd } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dashboard';
  loggedIn = false;
  loadingNewLink = false;
  newLink = {
    'url': '',
    'short_id': '',
  }
  newLinkMessage = '';

  constructor(
    authGuard: AuthGuardService,
    private requests: Requests,
    private router: Router,
  ) {
    this.router.events.subscribe(data => {
      if (data instanceof NavigationEnd) {
        this.loggedIn = authGuard.isLoggedIn();
      }
    })
  }

  async submitNewLink() {
    this.loadingNewLink = true;
    let response = await this.requests.post("/links", this.newLink);
    this.loadingNewLink = false;

    if (response.status != "ok") {
      this.newLinkMessage = response.message;
      return
    }
    this.newLink.url = '';
    this.newLink.short_id = '';
    this.newLinkMessage = '';
    $("#newLinkModal").modal('hide');
    this.router.navigateByUrl("/links/" + response.result.short_id);
  }

  logout() {
    delAccessToken();
    delRefreshToken();
    this.loggedIn = false;

    this.router.navigateByUrl("/login");
    this.requests.post("/logout", {
      refresh_token: getRefreshToken(),
    });
  }
}
