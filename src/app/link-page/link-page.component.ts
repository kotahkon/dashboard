import { Component, OnInit, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { Requests } from 'src/providers/requests';
import { ActivatedRoute } from '@angular/router';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-link-page',
  templateUrl: './link-page.component.html',
  styleUrls: ['./link-page.component.scss']
})
export class LinkPageComponent implements OnInit {
  objectKeys = Object.keys;
  days = '7';  
  statDayName = 'In past 7 days';

  loading = {
    'summary': true,
    'charts': true,
  };
  code = '';
  summary = [0, 0, 0];
  colors = [
    'red',
    'green',
    'blue',
    'yellow',
    'orange',
    'purple',
    'darkturquoise',
    'gold',
    'maroon',
    'midnightblue',
    'yellowgreen',
  ]

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'right',
    },
  };
  public pieChartType: ChartType = 'pie';

  public browserChartLabels: Array<any> = [];
  public browserChartData: number[] = [];
  public browserChartColors = [
    {
      backgroundColor: [],
    }
  ];
  public platformChartLabels: Array<any> = [];
  public platformChartData: number[] = [];
  public platformChartColors = [
    {
      backgroundColor: [],
    }
  ];
  public deviceChartLabels: Array<any> = [];
  public deviceChartData: number[] = [];
  public deviceChartColors = [
    {
      backgroundColor: [],
    }
  ];
  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Unique Users' },
    { data: [], label: 'Total Users' },
  ];
  public lineChartLabels: Array<any> = [];
  public lineChartOptions: any = {
    animation: false,
    responsive: true,
  };
  public lineChartColors: Array<any> = [
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend: boolean = true;
  public lineChartType: string = "line";

  @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective>;

  constructor(
    private requests: Requests,
    private route: ActivatedRoute,
  ) {

  }

  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++)
      color += letters[Math.floor(Math.random() * 16)];
    return color;
  }

  load() {
    this.loading.summary = true;
    this.requests.get("/links/" + this.code).then(data => {
      this.summary = data.result;
      this.loading.summary = false;
    })
    this.resetCharts();
  }

  resetStats() {
    if (this.days == '0') this.statDayName = 'Today';
    else if (this.days == '1') this.statDayName = 'Yesterday';
    else {
      this.statDayName = `In past ${this.days} days`
    }
    this.resetCharts();
  }

  resetCharts() {
    this.loading.charts = true;
    Promise.all([
      this.requests.get("/charts/link/" + this.code + "/pie", {
        days: this.days,
        unique: true,
      }),
      this.requests.get("/charts/link/" + this.code + "/line", {
        days: this.days,
        unique: true,
      }),
      this.requests.get("/charts/link/" + this.code + "/line", {
        days: this.days,
        unique: false,
      }),
    ]).then(response => {
      let pieResponse = response[0];
      let lineResponse = [response[1], response[2]];
      this.platformChartLabels.length = 0;
      this.platformChartData.length = 0;
      this.platformChartColors[0].backgroundColor.length = 0;
      this.objectKeys(pieResponse.result.platforms).forEach((key, index) => {
        let value = pieResponse.result.platforms[key];
        this.platformChartLabels.push(key);
        this.platformChartData.push(value);
        this.platformChartColors[0].backgroundColor.push(this.colors[index]);
      });

      this.deviceChartLabels.length = 0;
      this.deviceChartData.length = 0;
      this.deviceChartColors[0].backgroundColor.length = 0;
      this.objectKeys(pieResponse.result.devices).forEach((key, index) => {
        let value = pieResponse.result.devices[key];
        this.deviceChartLabels.push(key);
        this.deviceChartData.push(value);
        this.deviceChartColors[0].backgroundColor.push(this.colors[index]);
      });

      this.browserChartLabels.length = 0;
      this.browserChartData.length = 0;
      this.browserChartColors[0].backgroundColor.length = 0;
      this.objectKeys(pieResponse.result.browsers).forEach((key, index) => {
        let value = pieResponse.result.browsers[key];
        this.browserChartLabels.push(key);
        this.browserChartData.push(value);
        this.browserChartColors[0].backgroundColor.push(this.colors[index]);
      });

      this.lineChartData[0].data.length = 0;
      this.lineChartLabels.length = 0;
      for (var j = lineResponse[0].result.length - 1; j >= 0; j--) {
        var element = lineResponse[0].result[j];
        this.lineChartData[0].data.push(element.count);
        this.lineChartLabels.push(element.date);
      }
      this.lineChartData[1].data.length = 0;
      for (var j = lineResponse[1].result.length - 1; j >= 0; j--) {
        var element = lineResponse[1].result[j];
        this.lineChartData[1].data.push(element.count);
      }
      this.charts.forEach((child) => {
        child.chart.update()
      });
      this.loading.charts = false;
    });
  }

  ngOnInit() {
    this.route.params.subscribe(data => {
      this.code = data["code"];
      this.load();
    });
  }

}
