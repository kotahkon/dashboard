import { Component, OnInit } from '@angular/core';
import { Requests } from 'src/providers/requests';
import { setAccessToken, setRefreshToken } from 'src/auth/auth-token';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  loading = false;
  message = '';
  loginRequest = {
    username: '',
    password: '',
    captcha: '',
  }
  valid_captcha = false;
  recaptchaSiteKey = environment.recaptchaSiteKey;

  constructor(
    private requests: Requests,
  ) {

  }

  resolved(ev) {
    this.valid_captcha = ev != null;
  }

  async submit() {
    this.loading = true;
    let response = await this.requests.post("/auth/login", this.loginRequest);
    if (response.status != "ok") {
      this.loading = false;
      this.message = response.message;
      return;
    }
    
    setAccessToken(response.result.access_token);
    setRefreshToken(response.result.refresh_token);

    location.href = "/";  
  }

  ngOnInit() {
  }

}
