import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexPageComponent } from './index-page/index-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthGuardService } from 'src/auth/auth-gaurd.service';
import { RegisterPageComponent } from './register-page/register-page.component';
import { LinksPageComponent } from './links-page/links-page.component';
import { LinkPageComponent } from './link-page/link-page.component';


const routes: Routes = [
  {
    path: "dashboard",
    component: IndexPageComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: "links",
    component: LinksPageComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: "links/:code",
    component: LinkPageComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: "login",
    component: LoginPageComponent,
  },
  {
    path: "register",
    component: RegisterPageComponent,
  },
  { path: '**', redirectTo: '/dashboard' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
