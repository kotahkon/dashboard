import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { JwtModule } from '@auth0/angular-jwt';
import { getAccessToken } from 'src/auth/auth-token';
import { IndexPageComponent } from './index-page/index-page.component';
import { AuthService } from 'src/auth/auth.service';
import { AuthGuardService } from 'src/auth/auth-gaurd.service';
import { RegisterPageComponent } from './register-page/register-page.component';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LinksPageComponent } from './links-page/links-page.component';
import { LinkPageComponent } from './link-page/link-page.component';
import { ChartsModule } from 'ng2-charts';
import { MomentPipe } from 'src/pipes/date';

@NgModule({
  declarations: [
    MomentPipe,
    AppComponent,
    LoginPageComponent,
    IndexPageComponent,
    RegisterPageComponent,
    LinksPageComponent,
    LinkPageComponent,
  ],
  imports: [
    LoadingBarRouterModule,
    LoadingBarModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: getAccessToken,
      }
    }),
    RecaptchaModule,
    RecaptchaFormsModule,
    ChartsModule
  ],
  providers: [
    AuthGuardService,
    AuthService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
