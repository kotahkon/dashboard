import { Component, OnInit } from '@angular/core';
import { Requests } from 'src/providers/requests';

@Component({
  selector: 'app-links-page',
  templateUrl: './links-page.component.html',
  styleUrls: ['./links-page.component.scss']
})
export class LinksPageComponent implements OnInit {
  loading = true;
  links = [];

  filter = {
    'direction': 'desc',
    'order': 'created_at',
    'search': '',
  }

  constructor(
    private requests: Requests,
  ) {
    this.reset();
  }

  async reset() {
    this.loading = true;
    let response = await this.requests.get("/links", this.filter);
    this.links = response.result;
    this.loading = false;
  }

  ngOnInit() {
  }

}
