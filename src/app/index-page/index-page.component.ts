import { Component, OnInit, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { Requests } from 'src/providers/requests';
import { BaseChartDirective } from "ng2-charts";
import { ChartDataSets, ChartOptions } from 'chart.js';

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.scss']
})
export class IndexPageComponent implements OnInit {
  loading = false;
  days = 7;

  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Total requests' },
  ];
  public lineChartLabels: Array<any> = [];
  public lineChartOptions: any = {
    animation: false,
    responsive: true,
  };
  public lineChartColors: Array<any> = [
    {
      backgroundColor: "rgba(148,159,177,0.2)",
      borderColor: "rgba(148,159,177,1)",
      pointBackgroundColor: "rgba(148,159,177,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(148,159,177,0.8)"
    },
  ];
  public lineChartLegend: boolean = true;
  public lineChartType: string = "line";

  @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective>;

  constructor(
    private requests: Requests,
  ) {
    this.load();
  }

  async load() {
    this.loading = true;
    let response = await this.requests.get("/charts/overall?days=" + this.days);
    var length = response.result.output.length;
    this.lineChartData[0].data = [];
    this.lineChartLabels = [];
    for (var j = length - 1; j >= 0; j--) {
      var element = response.result.output[j];
      this.lineChartData[0].data.push(element.count);
      this.lineChartLabels.push(element.date);
    }
    this.charts.forEach((child) => {
      child.chart.update()
    });
    this.loading = false;
  }

  ngOnInit() {
  }

}
